unit uBitDemo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  bithelpers;

type

  { TForm1 }

  TForm1 = class(TForm)
    BooleanBitTestBtn: TButton;
    TmpFormatBtn: TButton;
    ChangeFormatBtn: TButton;
    OverlaysTestBtn: TButton;
    QuadwordBitTestBtn: TButton;
    LongwordBitTestBtn: TButton;
    WordBitTestBtn: TButton;
    ByteBitTestBtn: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    procedure BooleanBitTestBtnClick(Sender: TObject);
    procedure ByteBitTestBtnClick(Sender: TObject);
    procedure ChangeFormatBtnClick(Sender: TObject);
    procedure LongwordBitTestBtnClick(Sender: TObject);
    procedure OverlaysTestBtnClick(Sender: TObject);
    procedure QuadwordBitTestBtnClick(Sender: TObject);
    procedure TmpFormatBtnClick(Sender: TObject);
    procedure WordBitTestBtnClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BooleanBitTestBtnClick(Sender: TObject);
var
  MyBool: boolean;
begin
  MyBool := true;
  Memo1.Append(MyBool.ToOneZeroString);
  Memo1.Append(MyBool.ToOnOffString); // default is scfUnchangedCase and can be ommited
  Memo1.Append(MyBool.ToOnOffString(scfLowerCase)); // we do not want to use default else
  Memo1.Append(MyBool.ToTrueFalseString(scfUpperCase));
  Memo1.Append(MyBool.ToString('OnState', 'OffState')); // true/false custom strings
  Memo1.Append(MyBool.ToString('Укључено', 'Искључено', scfUpperCase)); // when case and unicode matter
  Memo1.Append('');
end;

procedure TForm1.ByteBitTestBtnClick(Sender: TObject);
var
  MyByte: byte;
begin
  MyByte.Clear;                                  // %00000000 MyByte equals 0
  MyByte.Bit[0] := true;                         // %00000001 MyByte equals 1
  MyByte.Bit[2] := true;                         // %00000101 MyByte equals 5
  Memo1.Append(MyByte.ToString);
  Memo1.Append('$' + MyByte.ToHexString);
  Memo1.Append(MyByte.ToBinString(lzHideLeadingZeros));   // hide leading zeros
  Memo1.Append(MyByte.ToBinString);                       // show leading zeros
  Memo1.Append('');
end;

procedure TForm1.WordBitTestBtnClick(Sender: TObject);
var
  MyWord: word;
begin
  MyWord.Clear;                  // %0000000000000000 MyWord equals 0
  MyWord.Byte[0] := 2;           // %0000000000000010 MyWord equals 2
  MyWord.Byte[1] := 1;           // %0000000100000010 MyWord equals 258 (2 + 256)
  MyWord.Byte[1].Bit[7] := true; // %0000000100000010 MyWord equals 258 (Beware!!! This DOES NOT set a bit in MyWord !!!)
  MyWord.Bit[10] := true;        // %0000010100000010 MyWord equals 1282 (258 + 2^10)
  Memo1.Append(MyWord.ToString);
  Memo1.Append('$' + MyWord.ToHexString);
  Memo1.Append(MyWord.ToBinString(lzHideLeadingZeros)); // hide leading zeros
  Memo1.Append(MyWord.ToBinString);                     // show leading zeros
  Memo1.Append('');
end;

procedure TForm1.LongwordBitTestBtnClick(Sender: TObject);
var
  MyLongword: longword;
begin
  MyLongword.Clear;                  // %00000000000000000000000000000000 MyLongword equals 0
  MyLongword.Word[0] := 250;         // %00000000000000000000000011111010 MyLongword equals 250
  MyLongword.Word[1].Byte[0] := 100; // %00000000000000000000000011111010 MyLongword equals 250 (Beware!!! This DOES NOT set a byte in MyLongword !!!)
  MyLongword.Byte[1] := 4;           // %00000000000000000000010011111010 MyLongword equals 1274 (250 + 2^(8 + 2), 2^2 = 4)
  MyLongword.Bit[26] := true;        // %00000100000000000000010011111010 MyLongword equals 67110138 (1274 + 2^26)
  Memo1.Append(MyLongword.ToString);
  Memo1.Append('$' + MyLongword.ToHexString);
  Memo1.Append(MyLongword.ToBinString(lzHideLeadingZeros)); // hide leading zeros
  Memo1.Append(MyLongword.ToBinString);                     // show leading zeros
  Memo1.Append('');
end;

procedure TForm1.QuadwordBitTestBtnClick(Sender: TObject);
var
  MyQuadword: qword;
begin
  MyQuadword.Clear;                      // %0000000000000000000000000000000000000000000000000000000000000000 MyQuadword equals 0
  MyQuadword.Longword[0] := 12345;       // %0000000000000000000000000000000000000000000000000011000000111001 MyQuadword equals 12345
  MyQuadword.Longword[1].Word[0] := 100; // %0000000000000000000000000000000000000000000000000011000000111001 MyQuadword equals 12345 (Beware!!! This DOES NOT set a word in MyQuadword !!!)
  MyQuadword.Byte[3] := 2;               // %0000000000000000000000000000000000000010000000000011000000111001 MyQuadword equals 33566777 (12345 + 2^(8 + 8 + 8 + 2), 2^1 = 2)
  MyQuadword.Bit[50] := true;            // %0000000000000100000000000000000000000010000000000011000000111001 MyQuadword equals 1125899940409401 (33566777 + 2^50)
  Memo1.Append(MyQuadword.ToString);
  Memo1.Append('$' + MyQuadword.ToHexString);
  Memo1.Append(MyQuadword.ToBinString(lzHideLeadingZeros)); // hide leading zeros
  Memo1.Append(MyQuadword.ToBinString);                     // show leading zeros
  Memo1.Append('');
end;

procedure TForm1.OverlaysTestBtnClick(Sender: TObject);
var
  MyQuadOverlay: TQuadwordOverlay;
begin
  MyQuadOverlay.AsQuadword.Clear;
  MyQuadOverlay.AsByte[0] := 100;                  // 0000000000000000000000000000000000000000000000000000000001100100
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  MyQuadOverlay.AsLongword[1] := 1;                // 0000000000000000000000000000000100000000000000000000000001100100
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  MyQuadOverlay.AsQuadword.Bit[32] := false;       // 0000000000000000000000000000000000000000000000000000000001100100
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  MyQuadOverlay.AsWordOverlay[3].AsByte[1] := $FF; // 1111111100000000000000000000000000000000000000000000000001100100
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  MyQuadOverlay.AsWord[3].Byte[1].Bit[5] := false; // 1111111100000000000000000000000000000000000000000000000001100100   NO CHANGE !!! Bit is changed in a result byte, not in a byte that belongs to MyQuadOverlay
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  MyQuadOverlay.AsBit[63] := false;                // 0111111100000000000000000000000000000000000000000000000001100100
  Memo1.Append(MyQuadOverlay.AsQuadword.ToBinString);
  Memo1.Append('');
end;

procedure TForm1.TmpFormatBtnClick(Sender: TObject);
var
  MyQuadWord: qword;
  MyTmpFormat: THelperFormatSettings;
begin
  Memo1.Append('');
  MyQuadWord := 1125899940409401;
  Memo1.Append('MyQuadWord := ' + IntToStr(MyQuadWord) + ';');
  Memo1.Append('BinStr(MyQuadWord, 64);');
  Memo1.Append(BinStr(MyQuadWord, 64));

  MyTmpFormat.PrefixString      := '< ';
  MyTmpFormat.SufixString       := ' >';
  MyTmpFormat.ByteSeparator     := ' - ';
  MyTmpFormat.WordSeparator     := ' = ';
  MyTmpFormat.LongwordSeparator := '   ';
  MyTmpFormat.NibbleSeparator   := '.';
  Memo1.Append('');
  Memo1.Append('MyTmpFormat.PrefixString := "'      + MyTmpFormat.PrefixString + '";');
  Memo1.Append('MyTmpFormat.SufixString  := "'      + MyTmpFormat.SufixString + '";');
  Memo1.Append('MyTmpFormat.NibbleSeparator := "'   + MyTmpFormat.NibbleSeparator + '";');
  Memo1.Append('MyTmpFormat.ByteSeparator := "'     + MyTmpFormat.ByteSeparator + '";');
  Memo1.Append('MyTmpFormat.WordSeparator := "'     + MyTmpFormat.WordSeparator + '";');
  Memo1.Append('MyTmpFormat.LongwordSeparator := "' + MyTmpFormat.LongwordSeparator + '";');

  Memo1.Append(LineEnding + 'MyQuadword.ToBinString(MyTmpFormat, true);   // true is default so it can be ommited and leading zeros will still be shown');
  Memo1.Append(MyQuadword.ToBinString(MyTmpFormat, true));

  Memo1.Append(LineEnding + 'MyQuadword.Longword[1].ToBinString(MyTmpFormat);     // default true is ommited, show higher longword with leading zeros');
  Memo1.Append(MyQuadword.Longword[1].ToBinString(MyTmpFormat));

  Memo1.Append(LineEnding + 'MyQuadword.Word[3].ToBinString(MyTmpFormat, false);     // show highest word without leading zeros');
  Memo1.Append(MyQuadword.Word[3].ToBinString(MyTmpFormat, false));
  Memo1.Append('');
end;

procedure TForm1.ChangeFormatBtnClick(Sender: TObject);
begin
  if DefaultHelperFormatSettings.PrefixString = '' then
  begin // custom settings
    DefaultHelperFormatSettings.PrefixString      := '{';
    DefaultHelperFormatSettings.SufixString       := '}';
    DefaultHelperFormatSettings.NibbleSeparator   := '_';
    DefaultHelperFormatSettings.ByteSeparator     := '  ';
    DefaultHelperFormatSettings.WordSeparator     := '  ' + Chr(39) + '  '; // Chr(39) is single quote
    DefaultHelperFormatSettings.LongwordSeparator := '  "  ';
  end
  else
  begin // default settings
    DefaultHelperFormatSettings.PrefixString      := '';
    DefaultHelperFormatSettings.SufixString       := '';
    DefaultHelperFormatSettings.NibbleSeparator   := '';
    DefaultHelperFormatSettings.ByteSeparator     := '';
    DefaultHelperFormatSettings.WordSeparator     := '';
    DefaultHelperFormatSettings.LongwordSeparator := '';
  end;
  Memo1.Append('Changed DefaultHelperFormatSettings.PrefixString to "'      + DefaultHelperFormatSettings.PrefixString + '"');
  Memo1.Append('Changed DefaultHelperFormatSettings.SufixString to "'       + DefaultHelperFormatSettings.SufixString + '"');
  Memo1.Append('Changed DefaultHelperFormatSettings.NibbleSeparator to "'   + DefaultHelperFormatSettings.NibbleSeparator + '"');
  Memo1.Append('Changed DefaultHelperFormatSettings.ByteSeparator to "'     + DefaultHelperFormatSettings.ByteSeparator + '"');
  Memo1.Append('Changed DefaultHelperFormatSettings.WordSeparator to "'     + DefaultHelperFormatSettings.WordSeparator + '"');
  Memo1.Append('Changed DefaultHelperFormatSettings.LongwordSeparator to "' + DefaultHelperFormatSettings.LongwordSeparator + '"');
  Memo1.Append('');
end;

end.

